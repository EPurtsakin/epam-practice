def createList (n):
    a = list(range(1, n+1))
    for n, i in enumerate(a):
        if i%5 == 0 and i%3 == 0:
            i = "FizzBuzz"
        elif i%5 == 0:
            i = "Buzz"
        elif i%3 == 0:
            i = "Fizz"
        a[n] = i
    print(a)

createList(15)